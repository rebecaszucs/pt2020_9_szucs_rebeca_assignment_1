
package com.company;


import java.util.ArrayList;
import java.lang.String;
import java.util.Optional;

import static java.lang.Integer.parseInt;
import static java.lang.Double.parseDouble;

public class Polinom {

    private ArrayList<Monom> polinom;

    public Polinom() {
        polinom = new ArrayList<>();
    }

    public ArrayList<Monom> getMonom() {
        return polinom;
    }

    public void addMonom(Monom x) {
        polinom.add(x);
    }

    //prima daca se gasesc monomii cu acelasi exponent, in caz pozitiv, se face adunare coeficientilor
    //daca nu s-au gasit monomi cu acelasi exponent, monomul respectiv se adauga pur si simplu
    //in colectia de la polinom, urmand sa se afiseze
    public void adunare(Monom x) {
        Optional<Monom> equalMonom = findMonomWithExponent(x.getExp());
        if (equalMonom.isPresent()) {
            Monom m = equalMonom.get();
            m.setCoef(m.getCoef() + x.getCoef());
        } else {
            polinom.add(x);
        }
    }
    //prima daca se gasesc monomii cu acelasi exponent, in caz pozitiv, se face scaderea coeficientilor
    //daca nu s-au gasit monomi cu acelasi exponent, monomul respectiv se adauga pur si simplu
    //in colectia de la polinom, urmand sa se afiseze
    public void scadere(Monom x) {
        Optional<Monom> equalMonom = findMonomWithExponent(x.getExp());
        if (equalMonom.isPresent()) {
            Monom m = equalMonom.get();
            double result = m.getCoef() - x.getCoef();
            if (result == 0) {
                polinom.remove(m);
            } else {
                m.setCoef(result);
            }
        } else {
            polinom.add(x);
        }
    }
    //metoda de derivare e simpla, presupune doar parcurgerea fiecarui monom si ii modifica coeficientii respectiv exponentii
    //noua valoare a coef va fi egala cu vechea valoare a coef inmultita cu exponentul
    //exponentul descreste cu o unitate
    public void derivare() {
        polinom.forEach(m -> {
            m.setCoef(m.getExp() * m.getCoef());
            m.setExp(m.getExp() - 1);
        });
    }
    //in metoda de integrare, se parcurge fiecare monom si se modifica coef si exp
    //noua valoare a coef va fi vechia valoare impartita la val exp +1
    //exponentul va creste cu o unitate
    public void integrare() {
        polinom.forEach(m -> {
            m.setCoef(m.getCoef() / (m.getExp() + 1));
            m.setExp(m.getExp() + 1);
        });
    }
//am folosit aceasta metoda pt a gasi monoamele cu acelasi eponent
    private Optional<Monom> findMonomWithExponent(int exponent) {
        return polinom
                .stream()
                .filter(p -> p.getExp() == exponent)
                .findFirst();
    }
    // aceasta metoda face conversia polinomului care este data sub forma de string cu ajutorul replace-regex
    //se folosesc doua stringuri, in primul string va fi salvata valoarea coef monomului
    //in al foilea string va fi salvata valoarea exponentului
    //ulterior, coeficiietii din stringul a vor fi convertiti la Double, iar exp din stringul b vor fi convertiti la int
    public void convertire(String x) {
        x = x.replace("-", "+-");
        String[] arrOfStr = x.split("\\+");
        String[] a;
        String[] b;
        for (String q : arrOfStr) {
            a = q.split("\\*");
            b = a[1].split("\\^");
            Monom y = new Monom(parseDouble(a[0]), parseInt(b[1]));
            polinom.add(y);
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (Monom m : polinom) {
            String sign = m.getCoef() < 0 ? "" : "+";
            String current = sign + m.getCoef() + "*X^" + m.getExp();
            sb.append(current);
        }
        return sb.toString();
    }

    public void resetare()
    {
        polinom.clear();
    }
}



