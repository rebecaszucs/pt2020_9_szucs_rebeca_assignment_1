package com.company;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Operatii {

    //pt adunare si scadere este acelasi algoritm, Polinomul se parcurge si se adauga in colectia de Monoame
    // //fiecare monom al polinomului, iar Polinomul b efectueaza adunare/scaderea
    //apeland metoda adunare/scadere din clasa Polinom
    public Polinom addi(Polinom a, Polinom b)
    {
        Polinom rezultat= new Polinom();
        for (Monom m : a.getMonom())
            rezultat.addMonom(m);
        for (Monom m : b.getMonom())
            rezultat.adunare(m);
        return rezultat;
    }
 // pt derivare si integrare, se primeste ca parametru un Polinom care se parcurge si care se adauga in colectia de monoame
    //ulterior se va apela metoda derivare/integrare din clasa Polinom care va efectua operatiile
    //specifice pe fiecare monom al polinomului
    public Polinom derivare(Polinom a) {
        Polinom rezultat = new Polinom();
        for (Monom m : a.getMonom())
            rezultat.addMonom(m);
        rezultat.derivare();

        return rezultat;
    }

    public Polinom integrare(Polinom a) {
        Polinom rezultat = new Polinom();
        for (Monom m : a.getMonom())
            rezultat.addMonom(m);
        rezultat.integrare();

        return rezultat;
    }

    public Polinom scad(Polinom a, Polinom b)
    {
        Polinom rezultat= new Polinom();
        for (Monom m : a.getMonom())
            rezultat.addMonom(m);
        for (Monom m : b.getMonom())
            rezultat.scadere(m);
        if (rezultat.getMonom().isEmpty()) {
            rezultat.addMonom(new Monom(0, 0));
        }
        return rezultat;
    }
}