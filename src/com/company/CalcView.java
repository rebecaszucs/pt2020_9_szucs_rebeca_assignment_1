package com.company;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;

import static java.lang.String.valueOf;

class CalcView extends JFrame {
    //... Components
   /* private JTextField m_userInputTf = new JTextField(5);
    private JTextField m_totalTf     = new JTextField(20);
    private JButton    m_multiplyBtn = new JButton("Multiply");
    private JButton    m_clearBtn    = new JButton("Clear");
*/

    private JPanel Polinom;
    private JButton adunareButton=new JButton("Adunare");
    private JButton integrareButton= new JButton("Integrare");
    private JButton scadereButton= new JButton("Scadere");
    private JButton impartireButton= new JButton("Impartire");
    private JButton inmultireButton= new JButton("Inmultire");
    private JButton derivareButton= new JButton("derivare");
   // private JTextField textField1=new JTextField();
   // private JTextField textField2=new JTextField();
    private JTextField rezultat=new JTextField(30);
    private JTextField polinom1=new JTextField(15);
    private JTextField polinom2=new JTextField(15);


    private Model m_model;


    CalcView(Model model) {
        m_model = model;

       // rezultat.setText(valueOf(m_model.getValue()).toString());
       // rezultat.setEditable(false);
 model.reset();

        JPanel content = new JPanel();
        content.setLayout(new FlowLayout());
        content.add(new JLabel("Polinom1")); content.add(polinom1);
        content.add(new JLabel("Polinom2")); content.add(polinom2);
        content.add(adunareButton);
        content.add(integrareButton);
        content.add(scadereButton);
        content.add(impartireButton);
        content.add(inmultireButton);
        content.add(derivareButton);




        content.add(new JLabel("Rezultat"));
        //JTextField rezultat = new JTextField();
        //


        content.add(rezultat);



        this.setContentPane(content);
        this.pack();

        this.setTitle(" Calc");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }



    String getUserInput1() {
        return polinom1.getText();
    }

    String getUserInput2() {
        return polinom2.getText();
    }

    void setTotal(String newTotal) {
        rezultat.setText(newTotal);
    }



    void addListener(ActionListener addl) {
        adunareButton.addActionListener(addl);
    }

    void intrListener(ActionListener intl) {
        integrareButton.addActionListener(intl);
    }

    void scdListener(ActionListener scdl) {
        scadereButton.addActionListener(scdl);
    }

    void impListener(ActionListener impl) {
        impartireButton.addActionListener(impl);
    }

    void inmListener(ActionListener inml) {
        inmultireButton.addActionListener(inml);
    }

    void derivListener(ActionListener derivl) {
        derivareButton.addActionListener(derivl);
    }
}
