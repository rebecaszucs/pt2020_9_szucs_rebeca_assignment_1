package com.company;

public class Monom {
    private double coef;
    private int exp;
        public Monom(double coef, int exp) {
            this.exp = exp;
            this.coef = coef;
        }


    public double getCoef ()
            {
                return coef;
            }

    public void setCoef(double coef) { this.coef = coef; }

    public int getExp ()
            {
                return exp;
            }

    public void setExp(int exp) {
        this.exp = exp;
    }
}
