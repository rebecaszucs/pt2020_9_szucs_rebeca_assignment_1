package com.company;
import javax.swing.*;
public class Main {
    public static void main(String[] args)
    {
        // TestAdunare();
        //TestScadere();
        //TestDerivare();
        //TestIntegrare();
        Model model = new Model();
        CalcView view = new CalcView(model);
        Controller controller = new Controller(model, view);

        view.setVisible(true);

    }
/*
    private static void TestAdunare() {
        Polinom p1=new Polinom();
        Polinom p2=new Polinom();
        Operatii o = new Operatii();

        String str = "7*x^4-4*x^2+5*x^1-7*x^0";
        String poli = "7*x^4-4*x^2+5*x^1-7*x^0";
        p1.convertire(str);
        p2.convertire(poli);
        Polinom resultAddition=o.addi(p1,p2);
        System.out.println(resultAddition.toString());
        assert resultAddition.toString().equals("+14.0*X^4-8.0*X^2+10.0*X^1-14.0*X^0");
    }

    private static void TestScadere() {
        Polinom p1=new Polinom();
        Polinom p2=new Polinom();
        Operatii o = new Operatii();

        String str = "8*x^4-4*x^2+5*x^1-7*x^0";
        String poli = "7*x^4-4*x^2+5*x^1-7*x^0";
        p1.convertire(str);
        p2.convertire(poli);
        Polinom resultSubtract = o.scad(p1, p2);
        System.out.println(resultSubtract.toString());
        assert resultSubtract.toString().equals("+1.0*X^4");
    }

    private static void TestDerivare() {
        Polinom p1=new Polinom();
        Operatii o = new Operatii();

        String str = "2*x^2+2*x^1";
        p1.convertire(str);
        Polinom resultDer = o.derivare(p1);
        System.out.println(resultDer.toString());
        assert resultDer.toString().equals("+4.0*X^1+2.0*X^0");
    }

    private static void TestIntegrare() {
        Polinom p1=new Polinom();
        Operatii o = new Operatii();

        String str = "2*x^2+2*x^1";
        p1.convertire(str);
        Polinom resultDer = o.integrare(p1);
        System.out.println(resultDer.toString());
        assert resultDer.toString().equals("+0.6666666666666666*X^3+1.0*X^2");
    }
*/

}