package com.company;



import java.awt.event.*;

public class Controller {

    private Model m_model;
    private CalcView  m_view;


    Controller(Model model, CalcView view) {
        m_model = model;
        m_view  = view;


        view.addListener(new AdunareListener());
        view.intrListener(new IntegrareListener());
        view.scdListener(new ScadereListener());
       // view.impListener(new DerivareListener());
        //view.inmListener(new ClearListener());
        view.derivListener(new DerivareListener());
    }



    class AdunareListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
        String polinom1="";
        String polinom2="";
        polinom1= m_view.getUserInput1();
        polinom2=m_view.getUserInput2();
        Polinom pol1= new Polinom();
        pol1.convertire(polinom1);
        Polinom pol2= new Polinom();
        pol2.convertire(polinom2);
        m_model.adunarePoli(pol1, pol2);
        m_view.setTotal(m_model.getValue().toString());
        }
    }

    class ScadereListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            String polinom1="";
            String polinom2="";
            polinom1= m_view.getUserInput1();
            polinom2=m_view.getUserInput2();
            Polinom pol1= new Polinom();
            pol1.convertire(polinom1);
            Polinom pol2= new Polinom();
            pol2.convertire(polinom2);
            m_model.scaderePoli(pol1, pol2);
            m_view.setTotal(m_model.getValue().toString());
        }
    }

    class DerivareListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            String polinom1="";
            polinom1= m_view.getUserInput1();
            Polinom pol1= new Polinom();
            pol1.convertire(polinom1);
            m_model.derivarePoli(pol1);
            m_view.setTotal(m_model.getValue().toString());
        }
    }

    class IntegrareListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            String polinom1="";
            polinom1= m_view.getUserInput1();
            Polinom pol1= new Polinom();
            pol1.convertire(polinom1);
            m_model.integrarePoli(pol1);
            m_view.setTotal(m_model.getValue().toString());
        }
    }

   /* class InmultireListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            System.out.println("O sa fac si inmultirea poate");
        }
    }*/
}
