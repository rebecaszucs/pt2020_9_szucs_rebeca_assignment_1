package com.company;



public class Model {
   private Polinom resultt;
   private Operatii op;
    /** Constructor */
    Model() {
        resultt=new Polinom();
        op=new Operatii();
        reset();
    }

    public void reset() {
        resultt.resetare();
    }


    public void adunarePoli(Polinom a, Polinom b) {
        resultt=op.addi(a,b);
    }
    public void scaderePoli(Polinom a, Polinom b) {
        resultt=op.scad(a,b);
    }
    public void derivarePoli(Polinom a) {
        resultt=op.derivare(a);
    }
    public void integrarePoli(Polinom a) {
        resultt=op.integrare(a);
    }

    public Polinom getValue() {
        return resultt;
    }
}
